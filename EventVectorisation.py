"""We use here a vectorisation program to change an event into a vector
to be understood by the machine learning algo
The classification here is:
-1 not phishing
0 suspicious
1 phishing/verry sucpicious"""
import regex
import datetime
import json


class Events:
    def __init__(self, event):
        self.issuerCompagny = event["issuerCompagny"]
        self.name = event["name"]
        self.whois = event["whoisServer"]
        self.url = event["url"]
        try:
            self.cert = event["certIssuer"]
        except:
            self.cert = "None"
        self.dns = event["nameServers"]
        self.registrarDate = event["registrarDate"]
        self.lastUpdateDate = event["lastUpdateDate"]
        self.expirationDate = event["expirationDate"]
        self.organisation = event["organisation"]
        self.address = event["address"]
        self.city = event["city"]
        self.state = event["state"]
        self.zipcode = event["zipcode"]
        self.country = event["country"]
        self.emails = event["emails"]
        self.isHTTP = event["isHTTP"]
        self.pageTitle = event["pageTitle"]

    def registrar_safe(self):
        trusted_reg = ["MarkMonitor, Inc.", "RegistrarSafe, LLC", "CSC CORPORATE DOMAINS, INC.",
                       "RegistrarSafe, LLC", "Eurodns S.A.", "Gabia, Inc.", "Name.com, Inc.",
                       "GoDaddy.com, LLC", "Network Solutions, LLC", "Tucows Domains Inc.",
                       "PortsGroup AB", "GANDI SAS", "eName Technology Co.,Ltd."]
        if self.issuerCompagny in trusted_reg:
            return -1
        elif self.issuerCompagny:
            return 0
        else:
            return 1

    def whois_server(self):
        trusted_whois = ["whois.markmonitor.com", "whois.registrarsafe.com", "whois.corporatedomains.com",
                         "whois.35.com", "grs-whois.hichina.com", "whois.eurodns.com", "whois.gabia.com",
                         "whois.name.com", "whois.godaddy.com", "whois.comlaude.com", "whois.ename.com"]
        if self.whois in trusted_whois:
            return -1
        else:
            return 1

    def is_name(self):
        if self.name:
            return -1
        else:
            return 1

    def name_servers(self):
        array = self.dns
        if len(array) > 6:
            return -1
        elif 6 >= len(array) >= 3:
            return 0
        else:
            return 1

    def url_length(self):
        url = self.url
        length = len(str(url))
        if length < 54:
            return -1
        elif 54 <= length <= 75:
            return 0
        else:
            return 1

    def having_at_symbol(self):
        symbol = regex.findall(r'@', self.url)
        if len(symbol) == 0:
            return -1
        else:
            return 1

    def is_cert_clean(self):
        try:
            trusted_Auth = ['Comodo', 'Symantec', 'GoDaddy', 'GlobalSign', 'DigiCert', 'StartCom', 'Entrust', 'Verizon',
                            'Trustwave', 'Unizeto', 'Buypass', 'QuoVadis', 'Deutsche Telekom', 'Network Solutions',
                            'SwissSign', 'IdenTrust', 'Secom', 'TWCA', 'GeoTrust', 'Thawte', 'Doster', 'VeriSign']
            if self.cert in trusted_Auth:
                return -1
            elif self.cert not in trusted_Auth:
                return 0
            else:
                return 1
        except:
            return 1

    def is_domain_old(self):
        stipped_date = datetime.datetime.strptime(self.registrarDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (current_date - stipped_date).days
        if age > 360:
            return -1
        elif 360 >= age >= 180:
            return 0
        else:
            return 1

    def is_update(self):
        stipped_date = datetime.datetime.strptime(self.lastUpdateDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (current_date - stipped_date).days
        if age < 60:
            return -1
        elif 180 <= age <= 60:
            return 0
        else:
            return 1

    def expiration_date(self):
        stipped_date = datetime.datetime.strptime(self.lastUpdateDate, '%Y-%m-%d %H:%M:%S')
        current_date = datetime.datetime.now()
        age = (stipped_date - current_date).days
        if age > 1440:  # 4 years
            return -1
        elif 1440 >= age >= 1080:  # Between 2 and 4 years
            return 0
        else:
            return 1

    def is_organisation(self):
        if self.organisation:
            return -1
        else:
            return 1

    def is_address(self):
        if self.address:
            return -1
        else:
            return 1

    def is_city(self):
        if self.city:
            return -1
        else:
            return 1

    def is_state(self):
        if self.state:
            return -1
        else:
            return 1

    def is_zipcode(self):
        if self.zipcode:
            return -1
        else:
            return 1

    def is_country(self):
        malicious_countries = ['IN', 'ZA', 'SG',
                               'UA', 'ID', 'MY',
                               'RO', 'TR', 'RU']
        if self.country in malicious_countries:
            return 1
        else:
            return -1

    def is_email(self):
        if self.emails:
            return -1
        else:
            return 1

    def is_http(self):
        if self.isHTTP == "False":
            return -1
        else:
            return 1

    def is_page_title(self):
        try:
            if self.pageTitle in self.organisation:
                return -1
            elif self.pageTitle:
                return 0
            else:
                return 1
        except:
            return 1



if __name__ == '__main__':
    
    for i in tableau:
        event = Events(i)
        vector = [event.registrar_safe(), event.whois_server(), event.is_name(), event.name_servers(), event.url_length(),
                event.having_at_symbol(), event.is_cert_clean(), event.is_domain_old(), event.is_update(),
                event.expiration_date(), event.is_organisation(), event.is_address(), event.is_city(),
                event.is_state(), event.is_zipcode(), event.is_country(), event.is_email(), event.is_http(),
                event.is_page_title()
                ]
        print(vector)
