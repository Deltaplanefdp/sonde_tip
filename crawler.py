import argparse
import sched
import time
import threading

from Api import Api
from Link import CoreOp
from OpenPhish import Ophish
from Phishtank import Phish


class Prettyfy:
    """This class is used to take a dictionnary from the Link class
    and transform it into json.
    We first make a list of the dictionnary then transform the list into
    a nasted dictionnary before finally jsonifying it"""

    def __init__(self, phishtank_url, openphish_url):
        self.api = Api()
        self.lien = CoreOp()
        self.phish = Phish(phishtank_url)
        self.ophish = Ophish(openphish_url)

    def fetch_ophish_url(self):
        url = self.ophish
        for name in url.grab_phish_url():
            print("Thread 1")
            try:
                response = self.lien.merge_dicts(name)
                print(response)
                self.api.push_events(response)
            except Exception as e:
                print(str(type(e)) + ': ' + str(e))
        return

    def fetch_phish_url(self):
        print("Le100")
        url = self.phish
        url = url.open_url()
        print(url)
        for name in self.phish.grab_url_and_submitter():
            print("Thread 2")
            try:
                response = self.lien.merge_dicts(name)
                print(response)
                self.api.push_events(response)
            except Exception as e:
                print(str(type(e)) + ': ' + str(e))
        return

    def fetch_platform_url(self, namelist):
        for name in namelist:
            print("Thread 3")
            try:
                response = self.lien.merge_dicts(name)
                print(response)
                self.api.push_events(response)
            except Exception as e:
                print(str(type(e)) + ': ' + str(e))
        return

    def get_urls(self):
        return self.api.get_url()

    def init_api(self):
        parser = argparse.ArgumentParser(
            description='Check-Access Reporting.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            '--api-key',
            dest='api_key',
            action='store',
            help='Generate discrepancy report.',
        )
        args = parser.parse_args()
        self.api.set_api_key(args.api_key)
        print(args.api_key)
        if not self.api.check_connection():
            print("Invalid API Key\nPlease provide valid API Key")
            exit(0)
        return


if __name__ == '__main__':
    threads = []
    Prettyfy = Prettyfy("https://www.phishtank.com/phish_search.php?verified=u&active=y",
                        "https://openphish.com/feed.txt")
    Prettyfy.init_api()
    s = sched.scheduler(time.time, time.sleep)

    while True:
        namelist = Prettyfy.get_urls()

        #Define the threads
        thread_1 = threading.Timer(3600.0, Prettyfy.fetch_ophish_url)
        thread_2 = threading.Timer(900.0, Prettyfy.fetch_phish_url)
        thread_3 = threading.Timer(20.0, Prettyfy.fetch_platform_url, [namelist])

        #Set threads as deamons
        thread_1.setDaemon(True)
        thread_2.setDaemon(True)
        thread_3.setDaemon(True)

        #Start threads
        thread_1.start()
        thread_2.start()
        thread_3.start()


        #Wait threads 2 and 3 to finish
        #thread_2.join()
        thread_3.join()

"""
We join only thread 2 and 3 since 1 is the longest thread.

without join:
+---+---+------------------                     main-thread
    |   |
    |   +...........                            child-thread(short)
    +..................................         child-thread(long)

with join
+---+---+------------------***********+###      main-thread
    |   |                             |
    |   +...........join()            |         child-thread(short)
    +......................join()......         child-thread(long)

with join and daemon thread
+-+--+---+------------------***********+###     parent-thread
  |  |   |                             |
  |  |   +...........join()            |        child-thread(short)
  |  +......................join()......        child-thread(long)
  +,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,     child-thread(long + daemonized)

'-' main-thread/parent-thread/main-program execution
'.' child-thread execution
'#' optional parent-thread execution after join()-blocked parent-thread could 
    continue
'*' main-thread 'sleeping' in join-method, waiting for child-thread to finish
',' daemonized thread - 'ignores' lifetime of other threads;
    terminates when main-programs exits; is normally meant for 
    join-independent tasks
    
"""
