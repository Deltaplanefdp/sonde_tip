List of the dependencies:

argparse
sched
time
threading
csv
json
os
time
urllib
requests
bs4
python-whois
ssl
socket

Make sure to install the library python-whois and not the whois library.

How to run the probe:

Make sure that the OpenTIP platform is running on your server
You can run the probe by entering the following command in a prompt:

python3 crawler.py --api-key <api-key>

In the file Api.py make sure that the correct api.url (the IP address of the platform) is correctly suited to your needs.

The crawler will once in:
- 20 seconds - fetch the URLs from OpenTIP
- 15 minutes - fetch the URLs from PhishTank
- 60 minutes - fetch the URLs from OpenPhish feed