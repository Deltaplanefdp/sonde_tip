from Link import Link
from Api import Api
import json


class Prettyfy:
    """This class is used to take a dictionnary from the Link class
    and transform it into json.
    We first make a list of the dictionnary then transform the list into
    a nasted dictionnary before finally jsonifying it"""

    def __init__(self, begin_url):
        self.begin_url = begin_url

    def print_url(self):
        """This """
        url = Link(self.begin_url)
        url.open_url()
        result = []
        for name in url.grab_url_and_submitter():
            print(name)
            if url.whoisify(name):
                result.append(url.whoisify(name))
            else:
                continue
        return result

    def dictionnarify(self):
        json_list = self.print_url()
        data = json_list.copy()
        object = "No_"
        json_dict = {}
        for i in range(len(data)):
            json_dict[object + str(i)] = data[i]
        return json_dict


if __name__ == '__main__':
    url1 = Prettyfy("https://phishtank.com")
    response = url1.dictionnarify()
    # response = json.dumps(response)
    print(response)
    #api = Api()
    #api.response(response)
