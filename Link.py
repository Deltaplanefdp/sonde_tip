from urllib.parse import urlparse


import requests
from bs4 import BeautifulSoup
import ssl
import socket
from Whois import Whois


class CoreOp:
    """This class is the main class of the first probe.
    This is where the main investigations functions are done."""
    @staticmethod
    def get_title_and_form(url):
        tab = []
        dict = {}
        html_page = requests.get(url)
        soup = BeautifulSoup(html_page.content, 'html.parser')
        try:
            dict["pageTitle"] = soup.title.string
        except:
            dict["pageTitle"] = "None"
        try:
            form = soup.find('form')
            inputs = form.find_all('input')
            dict["formFields"] = Whois.extract_form_fields(form)
            """
            for formulaire in inputs:
                tab.append(formulaire)
            dict["formFields"] = tab"""
        except:
            dict["formFields"] = "None"
        return dict

    @staticmethod
    def whoisify(name):
        """We use this function to return a dictionnary composed
        by the Whois.py parametres (such as urls, registrars...)
        and connect to potential phishing page to see is there is a form
        or not.
        In case of exceptions we return a sample with only the url and domain name

        We use here socket to get the ip address, bs4 for the forms and the whois
        library to do a whois on the domain name."""
        url_name = name
        name2 = name
        name = urlparse(name)
        isHTTP = name.scheme
        pre_json = Whois.do_whois(str(name2))
        pre_json["url"] = url_name
        if isHTTP == 'http':
            pre_json["isHTTP"] = "True"
        else:
            pre_json["isHTTP"] = "False"
            try:
                ctx = ssl.create_default_context()
                ctx.check_hostname = False
                ctx.verify_mode = ssl.CERT_NONE
                s = ctx.wrap_socket(socket.socket(), server_hostname=name.netloc)
                s.connect((name.netloc, 443))
                cert = s.getpeercert()
                issuer = dict(x[0] for x in cert['issuer']) if 'issuer' in cert else None
                issued_by = issuer['commonName'] if issuer else "None"
                pre_json["certIssuer"] = issued_by
            except Exception as e:
                print(str(type(e)) + ': ' + str(e))
        try:
            pre_json["ipAddress"] = socket.gethostbyname(name.netloc)
        except:
            pre_json["ipAddress"] = "None"
        return pre_json

    @staticmethod
    def merge_dicts(name):
        x = CoreOp.whoisify(name)
        y = CoreOp.get_title_and_form(name)
        if x and y:
            z = {**x, **y}
        else:
            return x
        return z
