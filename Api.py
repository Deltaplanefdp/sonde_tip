import requests
import json


class Api:
    """This class is used to connect to the API and send the Json data
    from the Prettyfy class to the main platform"""

    def __init__(self):
        self.api_url = "http://127.0.0.1:8000"
        self.api_key = None

    def push_events(self, json_data):
        data = {"api_key": self.api_key,
                "event": json_data}
        # data = dict(api_key=self.api_key, json=json_data)
        response = requests.post(self.api_url + "/api/events", json=data)
        return response

    def check_connection(self):
        data = {"api_key": self.api_key}
        response = requests.post(self.api_url + "/api/", json=data)
        response = json.loads(response.content)
        if "error" in response:
            if response["error"] == 0:
                return True
        return False

    def set_api_key(self, api_key):
        self.api_key = api_key
        return api_key

    def get_url(self):
        data = {"api_key": self.api_key}
        response = requests.get(self.api_url + "/api/urls", json=data)
        response = response.content.decode("utf-8")
        response = response.split(',')
        return response
