import whois


class Whois:
    """This class defines the fields used in the dictionnary in the Link class.
    There are several fields filled by various functions."""

    @staticmethod
    def do_whois(name):
        name = whois.whois(name)
        dict = {"domainName": name.domain_name,
                "issuerCompagny": name.registrar,
                "nameServers": name.name_servers,
                "name": name.name,
                "whoisServer": name.whois_server,
                "status": name.status,
                "organisation": name.org,
                "address": name.address,
                "city": name.city,
                "state": name.state,
                "zipcode": name.zipcode,
                "country": name.country,
                "emails": name.emails,
                "dnssec": name.dnssec,
                "referral_url": name.referral_url,
                "url": name}
        try:
            dict["expirationDate"] = name.expiration_date.strftime("%Y-%m-%d %X")
        except AttributeError:
            dict["expirationDate"] = name.expiration_date[0].strftime("%Y-%m-%d %X")
        except:
            dict["expirationDate"] = "000-00-00 00:00:00"
        try:
            dict["registrarDate"] = name.creation_date.strftime("%Y-%m-%d %X")
        except AttributeError:
            dict["registrarDate"] = name.creation_date[0].strftime("%Y-%m-%d %X")
        except:
            dict["registrarDate"] = "000-00-00 00:00:00"
        try:
            dict["lastUpdateDate"] = name.updated_date.strftime("%Y-%m-%d %X")
        except AttributeError:
            dict["lastUpdateDate"] = name.updated_date[0].strftime("%Y-%m-%d %X")
        except:
            dict["lastUpdateDate"] = "000-00-00 00:00:00"
        return dict

    @staticmethod
    def extract_form_fields(soup):
        fields = {}
        for input in soup.findAll('input'):
            # ignore submit/image with no name attribute
            if input['type'] in ('submit', 'image') and not 'name' in input:
                continue
            # single element nome/value fields
            if input['type'] in ('text', 'hidden', 'password', 'submit', 'image'):
                value = ''
                if 'value' in input:
                    value = input['value']
                fields[input['name']] = value
                continue
            # checkboxes and radios
            if input['type'] in ('checkbox', 'radio'):
                value = ''
                if input.has_attr("checked"):
                    if input.has_attr('value'):
                        value = input['value']
                    else:
                        value = 'on'
                if 'name' in input and value:
                    fields[input['name']] = value
                if not 'name' in input:
                    fields[input['name']] = value
                continue
            assert False, 'input type %s not supported' % input['type']
        # textareas
        for textarea in soup.findAll('textarea'):
            fields[textarea['name']] = textarea.string or ''
        # select fields
        for select in soup.findAll('select'):
            value = ''
            options = select.findAll('option')
            is_multiple = select.has_key('multiple')
            selected_options = [
                option for option in options
                if option.has_key('selected')
            ]
            # If no select options, go with the first one
            if not selected_options and options:
                selected_options = [options[0]]
            if not is_multiple:
                assert (len(selected_options) < 2)
                if len(selected_options) == 1:
                    value = selected_options[0]['value']
            else:
                value = [option['value'] for option in selected_options]
            fields[select['name']] = value
        return fields


"""{"No_0":
{
{
  "domain_name": "CBKKBNN2.INFO",
  "registrar": "Registrar of domain names REG.RU LLC",
  "whois_server": "whois.reg.com",
  "referral_url": null,
  "updated_date": [
    "2019-02-18 17:54:53",
    "2019-02-18 00:00:00"
  ],
  "creation_date": [
    "2019-02-18 17:54:50",
    "2019-02-18 20:54:57"
  ],
  "expiration_date": "2020-02-18 17:54:50",
  "name_servers": [
    "NS1.HOSTING.REG.RU",
    "NS2.HOSTING.REG.RU",
    "ns1.hosting.reg.ru",
    "ns2.hosting.reg.ru"
  ],
  "status": [
    "clientTransferProhibited https://icann.org/epp#clientTransferProhibited",
    "serverTransferProhibited https://icann.org/epp#serverTransferProhibited",
    "addPeriod https://icann.org/epp#addPeriod",
    "clientTransferProhibited http://www.icann.org/epp#clientTransferProhibited"
  ],
  "emails": [
    "abuse@reg.ru",
    "jodevi@themailpro.net"
  ],
  "name": "John Smith",
  "org": "Ma company",
  "address": "Koshinka str 20",
  "city": "Moscow",
  "state": "Moscow",
  "zipcode": "123456",
  "country": "GI"
}
"""
